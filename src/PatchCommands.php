<?php

namespace Drush\Commands\patch;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Drush\patch\PatchEngine\ComposerPatchesEngine;
use Drush\patch\PatchEngine\Patch;
use Drush\patch\PatchEngine\PatchEngineInterface;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;
use GuzzleHttp\Client;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Process\Process;

/**
 * Class PatchCommands
 *
 * @package Drush\Commands
 */
class PatchCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * @var \Drush\patch\PatchEngine\PatchEngineInterface
   */
  protected $patchEngine;

  /**
   * @return \Drush\patch\PatchEngine\PatchEngineInterface
   */
  public function getPatchEngine() {
    if (!isset($this->patchEngine)) {
      $engines = [
        ComposerPatchesEngine::class,
      ];
      foreach ($engines as $engine) {
        $this->logger->debug(dt('Checking if patch engine !class is applicable.', ['!class' => $engine]));
        try {
          if ($engine::isApplicable()) {
            $this->patchEngine = new $engine($this->logger());
            $this->logger->debug(dt('Using patch engine !class.', ['!class' => $engine]));
            return $this->patchEngine;
          }
        }
        catch (\Exception $exception) {
          $this->logger->debug(dt("Unable to use patch engine !class: !message", ['!class' => $engine, '!message' => $exception->getMessage()]));
        }
      }
      throw new \RuntimeException(dt('No patch engines were available.'));
    }
    return $this->patchEngine;
  }

  /**
   * @param \Drush\patch\PatchEngine\PatchEngineInterface $engine
   *
   * @return $this
   */
  public function setPatchEngine(PatchEngineInterface $engine) {
    $this->patchEngine = $engine;
    return $this;
  }

  /**
   * Lists all patches for the project.
   *
   * @command patch:list
   * @aliases patch-list,patch:status,patch-status
   * @field-labels
   *   project: Project
   *   id: ID
   *   uri: URI
   *   source: Source
   *   status: Status
   * @default-fields project,id,source,status
   * @bootstrap root
   * @usage `drush patch-status
   *   Bash: Import SQL statements from a file into the current database.
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   */
  public function patchList($options = ['format' => 'table']) {
    $status = $this->getPatchEngine()->getPatchesStatus();

    $rows = [];
    foreach ($status as $patch) {
      $row = (array) $patch;
      if ($options['format'] === 'table') {
        switch ($patch->status) {
          case Patch::PATCH_APPLIED:
            $row['status'] = ucfirst($patch->status);
            break;
          case Patch::PATCH_NEEDS_UPDATE:
          case Patch::PATCH_NEEDS_REMOVAL:
          case Patch::PATCH_NOT_APPLIED:
            $row['status'] = '<fg=red;bg=black>' . ucfirst($patch->status) . '</>';
            break;
        }
      }
      else {
        $row['status'] = ucfirst($row['status']);
      }
      $rows[] = $row;
    }

    return new RowsOfFields($rows);
  }
  /**
   * Applies patches to a project or all projects.
   *
   * @command patch:apply
   * @aliases patch-apply
   * @param string $project The project name.
   * @validate-patched-project-name project
   * @bootstrap root
   * @usage `drush patch-apply
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function patchApply($project = NULL, $id = NULL) {
    if (isset($project)) {
      if (isset($id)) {
        $message = dt('Do you really want to apply patch !id to !project?', ['!id' => $id, '!project' => $project]);
      }
      else {
        $message = dt('Do you really want to apply all current patches to !project?', ['!projcet' => $project]);
      }
    }
    else {
      $message = dt('Do you really want to apply all current patches?');
    }
    if (!$this->io()->confirm($message)) {
      throw new UserAbortException();
    }

    $this->getPatchEngine()->apply($project, $id);
  }

  /**
   * Adds a patch to project.
   *
   * @command patch:add
   * @aliases patch-add
   * @param string $project The project name.
   * @param string $id The ID for the patch.
   * @param string $uri The URI to the patch to apply.
   * @option apply If provided will run composer update --lock.
   * @validate-patchable-project-name project
   * @validate-uri-exists uri
   * @bootstrap root
   * @usage `drush patch-status
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function patchAdd($project, $id, $uri, $options = ['apply' => NULL]) {
    if (!$this->io()->confirm(dt('Do you really want to add the patch "!id" from !uri to !project?', ['!id' => $id, '!uri' => $uri, '!project' => $project]))) {
      throw new UserAbortException();
    }

    $engine = $this->getPatchEngine();
    $patch = new Patch();
    $patch->project = $project;
    $patch->id = $id;
    $patch->uri = $uri;
    $process = $this->processManager()->process([]);
    $engine->addPatch($patch, $process);
    $engine->save();

    if (!isset($options['apply'])) {
      $options['apply'] = $this->io()->confirm(dt('Do you want to update the !project code with the new patch?', ['!project' => $project]));
    }

    if ($options['apply']) {
      $this->logger->notice(dt('Running !command to add patch.', ['!command' => $process->getCommandLine()]));
      $process->mustRun($process->showRealtime());
      $this->output()->writeln($process->getOutput());
    }
  }

  /**
   * @hook interact patch:add
   */
  public function interactPatchAdd(InputInterface $input) {
    if ($input->getArgument('project') === NULL) {
      $projects = $this->getPatchEngine()->getAllProjects();
      // Do not use $this->io()->choice() here since the list of projects could
      // be potentially very long.
      $question = new Question(dt('Choose a project for patching'));
      $question->setAutocompleterValues($projects);
      $project = $this->io()->askQuestion($question);
      $input->setArgument('project', $project);
    }

    if ($input->getArgument('uri') === NULL) {
      $input->setArgument('uri', $this->io()->ask(dt('Patch file (can be a file using relative or absolute path or a URL)')));
    }

    if ($input->getArgument('id') === NULL) {
      $id = $this->io()->ask(dt('A unique, descriptive ID for the patch'), basename($input->getArgument('uri')));
      $input->setArgument('id', $id);
    }
  }

  /**
   * Adds a patch to project from a Drupal.org issue.
   *
   * @command patch:add:drupal
   * @aliases patch-add-drupal
   * @param string $issue_url The URI to the patch to apply.
   * @option apply If provided will run composer update --lock.
   * @validate-url-drupal-issue issue_url
   * @bootstrap root
   * @usage `drush patch-status
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function patchAddDrupal($issue_url, $options = ['apply' => NULL]) {
    if ($suggested_patches = $this->getSuggestedPatchesFromUri($issue_url)) {
      $options = [];
      foreach ($suggested_patches as $patch) {
        $options[] = $patch->label ?? $patch->uri;
      }
      $selected = $this->io()->choice(dt('Chose the patch to use'), $options);
      $patch = $suggested_patches[$selected];
      return $this->patchAdd($patch->project, $patch->id, $patch->uri, $options);
    }
    else {
      return dt('No patches found at !url', ['!url' => $issue_url]);
    }
  }

  /**
   * @hook interact patch:add:drupal
   */
  public function interactPatchAddDrupal(InputInterface $input) {
    if ($input->getArgument('issue_url') === NULL) {
      $input->setArgument('issue_url', $this->io()->ask(dt('Drupal.org issue URL')));
    }
  }

  /**
   * Imports an exported database dump to the database.
   *
   * @command patch:update
   * @aliases patch-update
   * @param string $project The project name.
   * @param string $id The patch identifier.
   * @param string $uri The updated patch URI.
   * @option apply If provided will run composer update --lock.
   * @validate-patch project patch_id
   * @validate-uri-exists uri
   * @bootstrap root
   * @usage `drush patch-status
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function patchUpdate($project, $id, $uri, $options = ['apply' => NULL]) {
    if (!$this->io()->confirm(dt('Do you really want to update the patch !id from !project?', ['!id' => $id, '!project' => $project]))) {
      throw new UserAbortException();
    }

    $engine = $this->getPatchEngine();
    $patch = $engine->getPatch($project, $id);
    // @todo Add validation if URI doesn't change.
    $patch->uri = $uri;
    // No need to call $engine->setPatches() here since we are modifying an
    // existing Patch class in the array.
    $engine->save();

    if (!isset($options['apply'])) {
      $options['apply'] = $this->io()->confirm(dt('Do you want to update the !project code with the updated patch?', ['!project' => $project]));
    }

    if ($options['apply']) {
      $this->logger->notice(dt('Running !command to update patch.', ['!command' => $process->getCommandLine()]));
      $process->mustRun($process->showRealtime());
      $this->output()->writeln($process->getOutput());
    }
  }

  /**
   * @hook interact patch:update
   */
  public function interactPatchUpdate(InputInterface $input) {
    $engine = $this->getPatchEngine();

    if ($input->getArgument('project') === NULL) {
      $projects = $engine->getPatchedProjects();
      $selected = $this->io()->choice(dt('Chose the project'), $projects);
      $input->setArgument('project', $projects[$selected]);
    }

    $patches = $engine->getProjectPatches($input->getArgument('project'));
    if ($input->getArgument('id') === NULL) {
      $selected = $this->io()->choice(dt('Chose the patch to update'), $patches);
      $input->setArgument('id', $patches[$selected]->id);
    }

    $patch = $engine->getPatch($input->getArgument('project'), $input->getArgument('id'));
    if ($input->getArgument('uri') === NULL) {
      $uri = $this->io()->ask(dt('What is the new URL of the patch?'), $patch ? $patch->url : NULL);
      $input->setArgument('uri', $uri);
    }
  }

  /**
   * Remove a patch from a project.
   *
   * @command patch:remove
   * @aliases patch-remove
   * @param string $project The project name.
   * @param string $id The patch identifier.
   * @option apply If provided will run composer update --lock.
   * @validate-patched-project
   * @validate-patch
   * @bootstrap root
   * @usage `drush patch-status
   *   Bash: Import SQL statements from a file into the current database.
   */
  public function patchRemove($project, $id, $options = ['apply' => NULL]) {
    if (!$this->io()->confirm(dt('Do you really want to remove the patch !id from !project?', ['!id' => $id, '!project' => $project]))) {
      throw new UserAbortException();
    }

    $engine = $this->getPatchEngine();
    $patch = $engine->getPatch($project, $id);
    $status = $patch->getStatus($engine->getAppliedPatches());
    $process = $this->processManager()->process([]);
    $engine->removePatch($patch, $process);
    $engine->save();

    if ($status === Patch::PATCH_NOT_APPLIED) {
      $this->logger()->notice(dt('Patch !id for !project is not applied so skipping code update.', ['!project' => $project, '!id' => $id]));
      return;
    }

    if (!isset($options['apply'])) {
      $options['apply'] = $this->io()->confirm(dt('Do you want to update the !project code with the removed patch?', ['!project' => $project]));
    }

    if ($options['apply']) {
      $this->logger->notice(dt('Running !command to remove patch.', ['!command' => $process->getCommandLine()]));
      $process->mustRun($process->showRealtime());
      $this->output()->writeln($process->getOutput());
    }
  }

  /**
   * @hook interact patch:remove
   */
  public function interactPatchRemove(InputInterface $input) {
    $engine = $this->getPatchEngine();

    if ($input->getArgument('project') === NULL) {
      $projects = $engine->getPatchedProjects();
      $selected = $this->io()->choice(dt('Chose the project'), $projects);
      $input->setArgument('project', $projects[$selected]);
    }

    $patches = $engine->getProjectPatches($input->getArgument('project'));
    if ($input->getArgument('id') === NULL) {
      $selected = $this->io()->choice(dt('Chose the patch to remove'), $patches);
      $input->setArgument('id', $patches[$selected]->id);
    }
  }

  /**
   * Validate that a config name is valid.
   *
   * If the argument to be validated is not named $config_name, pass the
   * argument name as the value of the annotation.
   *
   * @hook validate @validate-patchable-project-name
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *
   * @return \Consolidation\AnnotatedCommand\CommandError|null
   */
  public function validateProjectName(CommandData $commandData) {
    $arg_name = $commandData->annotationData()->get('validate-patchable-project-name', null) ?: 'project';
    $project = $commandData->input()->getArgument($arg_name);
    if (!in_array($project, $this->getPatchEngine()->getAllProjects())) {
      throw new \InvalidArgumentException(dt('The project !project is not installed.', ['!project' => $project]));
    }
  }

  /**
   * Validate that a config name is valid.
   *
   * If the argument to be validated is not named $config_name, pass the
   * argument name as the value of the annotation.
   *
   * @hook validate @validate-uri-exists
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *
   * @return \Consolidation\AnnotatedCommand\CommandError|null
   */
  public function validateUriExists(CommandData $commandData) {
    $arg_name = $commandData->annotationData()->get('validate-uri-exists', null) ?: 'uri';
    $uri = $commandData->input()->getArgument($arg_name);

    if (is_file($uri)) {
      return;
    }
    elseif ($this->isValidExternalUrl($uri)) {
      if (!in_array(pathinfo($uri, PATHINFO_EXTENSION), ['patch', 'diff'])) {
        throw new \InvalidArgumentException(dt('The patch URI !uri does not seem to be a patch file.', ['!uri' => $uri]));
      }
      $client = new Client();
      $response = $client->head($uri);
      $content_type = $response->getHeaderLine('content-type');
      if (strpos($content_type, 'text/plain') === FALSE) {
        throw new \InvalidArgumentException(dt('The patch URI !uri does not seem to be a patch file.', ['!uri' => $uri]));
      }
    }
    else {
      throw new \InvalidArgumentException(dt('The patch URI !uri is invalid.', ['!uri' => $uri]));
    }
  }

  /**
   * @hook validate @validate-url-drupal-issue
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *
   * @return \Consolidation\AnnotatedCommand\CommandError|null
   */
  public function validateDrupalIssueUrl(CommandData $commandData) {
    $arg_name = $commandData->annotationData()->get('validate-url-drupal-issue', null) ?: 'url';
    $url = $commandData->input()->getArgument($arg_name);

    if (!preg_match('#https://www.drupal.org/project/(\w+)/issues/(\d+)#', $url)) {
      throw new \InvalidArgumentException(dt('The URL !uri is not a valid Drupal.org issue URL.', ['!uri' => $url]));
    }
    else {
      $client = new Client();
      $response = $client->head($url);
      $content_type = $response->getHeaderLine('content-type');
      if (strpos($content_type, 'text/html') === FALSE) {
        throw new \InvalidArgumentException(dt('The URL !uri is not a valid Drupal.org issue URL.', ['!uri' => $url]));
      }
    }
  }

  /**
   * Validate that a config name is valid.
   *
   * If the argument to be validated is not named $config_name, pass the
   * argument name as the value of the annotation.
   *
   * @hook validate @validate-patched-project-name
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *
   * @return \Consolidation\AnnotatedCommand\CommandError|null
   */
  public function validatePatchedProjectName(CommandData $commandData) {
    $arg_name = $commandData->annotationData()->get('validate-patched-project-name', null) ?: 'project';
    $project = $commandData->input()->getArgument($arg_name);
    if (!in_array($project, $this->getPatchEngine()->getPatchedProjects())) {
      throw new \InvalidArgumentException(dt('The project !project does not have any patches.', ['!project' => $project]));
    }
  }

  /**
   * Validate that a config name is valid.
   *
   * If the argument to be validated is not named $config_name, pass the
   * argument name as the value of the annotation.
   *
   * @hook validate @validate-patch
   * @param \Consolidation\AnnotatedCommand\CommandData $commandData
   *
   * @return \Consolidation\AnnotatedCommand\CommandError|null
   */
  public function validatePatch(CommandData $commandData) {
    list($project_arg_name, $id_arg_name) = explode(' ', $commandData->annotationData()->get('@validate-patch', null) ?: 'project id');
    $project = $commandData->input()->getArgument($project_arg_name);
    $id = $commandData->input()->getArgument($id_arg_name);

    if (!$this->getPatchEngine()->getPatch($project, $id)) {
      throw new \InvalidArgumentException(dt('The project !project does not have patch !id.', ['!project' => $project, '!id' => $id]));
    }
  }

  protected function isValidExternalUrl($url) {
    return (bool) preg_match("
        /^                                                      # Start at the beginning of the text
        (?:ftp|https?):\/\/                                     # Look for ftp, http, or https schemes
        (?:                                                     # Userinfo (optional) which is typically
          (?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*      # a username or a username and password
          (?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@          # combination
        )?
        (?:
          (?:[a-z0-9\-\.]|%[0-9a-f]{2})+                        # A domain name or a IPv4 address
          |(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\])         # or a well formed IPv6 address
        )
        (?::[0-9]+)?                                            # Server port number (optional)
        (?:[\/|\?]
          (?:[\w#!:\.\?\+=&@$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})   # The path and query (optional)
        *)?
      $/xi", $url);
  }

  protected function isValidPatchUrl($url) {
    if (is_file($url)) {
      return TRUE;
    }
    elseif ($this->isValidExternalUrl($url) && in_array(pathinfo($url, PATHINFO_EXTENSION), ['patch', 'diff'])) {
      $client = new Client();
      $response = $client->head($url);
      $content_type = $response->getHeaderLine('content-type');
      return strpos($content_type, 'text/plain') !== FALSE;
    }
    return FALSE;
  }

  protected function getSuggestedIdFromUri($uri) {
    if ($this->isValidExternalUrl($uri)) {
      $client = new Client();
      $response = $client->get($uri);
      $crawler = new Crawler($response->getBody()->getContents());
      return $this->crawlForId($crawler);
    }

    return basename($uri);
  }

  protected function getSuggestedPatchesFromUri($uri) {
    $patches = [];

    if ($this->isValidExternalUrl($uri)) {
      $client = new Client();
      $response = $client->get($uri);
      $content_type = $response->getHeaderLine('content-type');
      if (strpos($content_type, 'text/html') !== FALSE) {
        if (preg_match('~https://www.drupal.org/project/(\w+)/issues/(\d+)~', $uri, $matches)) {
          if ($matches[1] === 'drupal') {
            $project = 'drupal/core';
          }
          else {
            $project = 'drupal/' . $matches[1];
          }
          $crawler = new Crawler($response->getBody()->getContents());
          $id = $this->crawlForId($crawler);
          $table_rows = $crawler->filter('table#extended-file-field-table-field-issue-files > tbody > tr:not(.hidden-property):not(.pift-test-info)');
          if ($table_rows->count()) {
            $table_rows->each(function (Crawler $row) use (&$patches, $project, $id) {
              $patch_url = $row->filter('td.extended-file-field-table-filename a')->getNode(0)->getAttribute('href');
              if ($this->isValidPatchUrl($patch_url)) {
                $patch = new Patch();
                $patch->project = $project;
                $patch->id = $id;
                $patch->uri = $patch_url;
                // @todo Add if tests are passing to the summary?
                $patch->label = basename($patch_url);
                $patches[] = $patch;
              }
            });
          }
        }
        elseif (preg_match('~^https://github\.com/([\w-]+)/([\w-]+)/pull/\d+$~i', $uri, $matches)) {
          $patch_url = $uri . '.diff';
          if ($this->isValidPatchUrl($patch_url)) {
            $crawler = new Crawler($response->getBody()->getContents());
            $id = $this->crawlForId($crawler);
            $patch = new Patch();
            $patch->project = $matches[1] . '/' . $matches[2];
            $patch->id = $id;
            $patch->uri = $patch_url;
            $patches[] = $patch;
          }
        }
        elseif (preg_match('~^https://gitlab\.com/([\w-]+)/([\w-]+)/merge_requests/\d+$~i', $uri, $matches)) {
          $patch_url = $uri . '.diff';
          if ($this->isValidPatchUrl($patch_url)) {
            $crawler = new Crawler($response->getBody()->getContents());
            $id = $this->crawlForId($crawler);
            $patch = new Patch();
            $patch->project = $matches[1] . '/' . $matches[2];
            $patch->id = $id;
            $patch->uri = $patch_url;
            $patches[] = $patch;
          }
        }
      }
    }

    return $patches;
  }

  protected function crawlForId(Crawler $crawler) {
    if ($title = $crawler->filterXpath("//head/meta[@property='og:title']")->getNode(0)->getAttribute('content')) {
      return $title;
    }
    elseif ($title = $crawler->filterXpath("//head/title")->text()) {
      return $title;
    }
  }

  protected function getPossiblePatchesFromUrl($url) {
    if (preg_match('~^https?://(?:www\.)?drupal\.org(/node/\d+(#comment-\d+)?)~i', $url, $matches)) {
      $patches = array();
      if ($request = @file_get_contents($url)) {
        $dom = @new \DOMDocument();
        @$dom->loadHTML($request);
        $xpath = new \DOMXpath($dom);

        foreach ($xpath->query("//table[@id='extended-file-field-table-field-issue-files']/tbody/tr[not(contains(concat(' ',normalize-space(@class),' '),' element-hidden '))]") as $element) {
          $file_xpath = $xpath->query("td[@class='extended-file-field-table-filename']//a", $element);
          if (!$file_xpath->length) {
            continue;
          }

          // If a comment number was provided, then filter the results to only
          // patches that match the comment.
          $cid_query = "td[@class='extended-file-field-table-cid']//a";
          if (!empty($matches[2])) {
            $cid_query .= "[@href='{$matches[1]}']";
          }
          $cid_xpath = $xpath->query($cid_query, $element);
          if (!$cid_xpath->length) {
            continue;
          }

          // Validate that this is actually a .patch or .diff file.
          $patch_url = $file_xpath->item(0)->getAttribute('href');
          if (!in_array(pathinfo($patch_url, PATHINFO_EXTENSION), array('patch', 'diff'))) {
            continue;
          }

          $size = $xpath->query("td[@class='extended-file-field-table-filesize']", $element)->item(0)->textContent;
          $author = $xpath->query("td[@class='extended-file-field-table-uid']//a", $element)->item(0)->textContent;
          $cid = ltrim($cid_xpath->item(0)->textContent, '#');
          $patches[$patch_url] = array(
            'patch' => $file_xpath->item(0)->textContent,
            'details' =>  sprintf("%10s by %s on comment %d", $size, $author, $cid),
          );
        }
      }
      return $patches;
    }

    if (preg_match('~^https://github\.com/[\w-]+/[\w-]+/pull/\d+$~i', $url, $matches)) {
      $patch_url = $url . '.diff';
      if (@file_get_contents($patch_url)) {
        return array($patch_url => $patch_url);
      }
    }

    if (preg_match('~^https://bitbucket\.org/([\w-]+)/([\w-]+)/pull-request/(\d+)~i', $url, $matches)) {
      $patch_url = "https://bitbucket.org/api/2.0/repositories/{$matches[1]}/{$matches[2]}/pullrequests/{$matches[3]}/diff";
      if (@file_get_contents($patch_url)) {
        return array($patch_url => $patch_url);
      }
    }

    return FALSE;
  }

}
