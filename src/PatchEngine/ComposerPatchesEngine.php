<?php

namespace Drush\patch\PatchEngine;

use Drush\Drush;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

class ComposerPatchesEngine extends PatchEngine {

  /**
   * @var \Drush\patch\PatchEngine\ComposerJsonFile
   */
  protected $composerFile;

  /**
   * @var \Drush\patch\PatchEngine\ComposerJsonFile
   */
  protected $lockFile;

  /**
   * @var \Drush\patch\PatchEngine\ComposerJsonFile
   */
  protected $patchesFile;

  /**
   * @var \Drush\patch\PatchEngine\Patch[]
   */
  protected $appliedPatches;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerInterface $logger) {
    parent::__construct($logger);
    $this->composerFile = $this->getComposerFile();
    $this->lockFile = $this->getLockFile();

    // The composer-patches package is required.
    if (!isset($this->composerFile['require']['cweagans/composer-patches'])) {
      throw new \RuntimeException("The package cweagans/composer-patches is not yet required in {$this->composerFile->getUri()}.");
    }

    $this->readPatches();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable() {
    $composer_json_uri = static::getComposerRoot() . '/composer.json';
    return is_file($composer_json_uri)
       && ($data = json_decode(file_get_contents($composer_json_uri), TRUE))
       && isset($data['require']['cweagans/composer-patches']);
  }

  protected function readPatches() {
    if (!empty($this->composerFile['extra']['patches-file'])) {
      $patches_file_uri = dirname($this->composerFile->getUri()) . '/' . $this->composerFile['extra']['patches-file'];
      // @todo Should this attempt to create the patches file if it does not exist?
      $this->patchesFile = new ComposerJsonFile($patches_file_uri);
      $this->logger->debug("Reading {$this->patchesFile->getUri()} for patches.");
      $this->patches = $this->unserializePatches($this->patchesFile['patches'] ?? [], ['source' => basename($this->patchesFile->getUri())]);
    }
    else {
      $this->logger->debug("Reading {$this->composerFile->getUri()} for patches.");
      $this->patches = $this->unserializePatches($this->composerFile['extra']['patches'] ?? [], ['source' => basename($this->composerFile->getUri())]);
    }

    $this->mergeDependenciesPatches();
  }

  protected function mergeDependenciesPatches() {
    if (empty($this->composerFile['extra']['enable-patching'])) {
      return;
    }

    $ignored_patches = $this->composerFile['extra']['patches-ignore'] ?? [];

    // Merge in sub-project patches.
    $this->logger->debug("Reading {$this->lockFile->getUri()} for additional patches from dependencies.");
    foreach ($this->lockFile['packages'] as $lock_package) {
      if (!empty($lock_package['extra']['patches'])) {
        $patches = $lock_package['extra']['patches'];

        // Process ignored patches.
        foreach ($patches as $project => $project_patches) {
          if (isset($ignored_patches[$lock_package['name']][$project])) {
            $patches[$project] = array_diff($project_patches, $ignored_patches[$lock_package['name']][$project]);
          }
        }

        $this->patches = array_merge($this->patches, $this->unserializePatches($patches, ['source' => $lock_package['name']]));
      }
    }
  }

  /**
   * Gets the root composer.json file directory.
   *
   * @return string
   */
  protected static function getComposerRoot() {
    return Drush::bootstrapManager()->drupalFinder()->getComposerRoot();
  }

  /**
   * Gets the root composer.json file.
   *
   * @return \Drush\patch\PatchEngine\ComposerJsonFile
   */
  public function getComposerFile() {
    return new ComposerJsonFile(static::getComposerRoot() . '/composer.json');
  }

  /**
   * Gets the root composer lock file.
   *
   * @return \Drush\patch\PatchEngine\ComposerJsonFile
   */
  public function getLockFile() {
    return new ComposerJsonFile(static::getComposerRoot() . '/composer.lock');
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    if (isset($this->patchesFile)) {
      $this->patchesFile['patches'] = $this->serializePatches($this->patches);
      $this->patchesFile->write();
      $this->logger->notice(dt('Saved !file.', ['!file' => $this->patchesFile->getUri()]));
    }
    else {
      $this->composerFile->read();
      $this->composerFile['extra']['patches'] = $this->serializePatches($this->patches);
      $this->composerFile->write();
      $this->logger->notice(dt('Saved !file.', ['!file' => $this->composerFile->getUri()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAppliedPatches() {
    if (!isset($this->appliedPatches)) {
      $this->appliedPatches = [];
      foreach ($this->lockFile['packages'] as $lock_package) {
        if (isset($lock_package['extra']['patches_applied'])) {
          $this->appliedPatches[$lock_package['name']] = $lock_package['extra']['patches_applied'];
        }
      }
      $this->appliedPatches = $this->unserializePatches($this->appliedPatches);
    }
    return $this->appliedPatches;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllProjects() {
    static $package_names;
    if (!isset($package_names)) {
      $this->logger->notice(__METHOD__);
      $lock_data = $this->lockFile->read();
      foreach ($lock_data['packages'] as $lock_package) {
        $package_names[] = $lock_package['name'];
      }
    }
    sort($package_names);
    return $package_names;
  }

  /**
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  protected function unserializePatches(array $data, array $properties = []) {
    $patches = [];
    foreach ($data as $package_name => $project_patches) {
      foreach ($project_patches as $id => $uri) {
        $patch = new Patch();
        $patch->project = $package_name;
        $patch->id = $id;
        $patch->uri = $uri;
        foreach ($properties as $key => $value) {
          $patch->{$key} = $value;
        }
        $patches[] = $patch;
      }
    }
    return $patches;
  }

  protected function getSerializedSources() {
    $sources = [];
    $sources[] = basename($this->composerFile->getUri());
    if (isset($this->patchesFile)) {
      $sources[] = basename($this->patchesFile->getUri());
    }
    return $sources;
  }

  /**
   * @param \Drush\patch\PatchEngine\Patch[] $patches
   *
   * @return array
   */
  protected function serializePatches(array $patches) {
    $data = [];
    foreach ($patches as $patch) {
      if (!empty($patch->source) && !in_array($patch->source, $this->getSerializedSources())) {
        continue;
      }
      if (!isset($data[$patch->project])) {
        $data[$patch->project] = [];
      }
      $data[$patch->project][$patch->id] = $patch->uri;
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function addPatch(Patch $patch, Process $process) {
    parent::addPatch($patch, $process);
    $this->setProcess($process);
  }

  /**
   * {@inheritdoc}
   */
  public function removePatch(Patch $patch, Process $process) {
    parent::removePatch($patch, $process);
    $this->setProcess($process);
  }

  protected function setProcess(Process $process) {
    // @todo Should this use composer update project --lock?
    $command = ['composer', 'update', '--lock'];
    $process->setCommandLine($command);
    $process->setWorkingDirectory(dirname($this->composerFile->getUri()));
    // Clear the applied patches since the composer.lock will be updated.
    $this->appliedPatches = NULL;
  }

  public function setUp(Process $process) {
    $command = ['composer', 'require', 'cweagans/composer-patches'];
    $process->setCommandLine($command);
    $process->setWorkingDirectory(dirname($this->composerFile->getUri()));
  }

}
