<?php

namespace Drush\patch\PatchEngine;

class ComposerJsonFile extends \ArrayObject {

  protected $uri;

  protected $indentation;

  const DEFAULT_INDENTATION = '    ';

  /**
   * Construct a new file object.
   */
  public function __construct($uri) {
    if (!is_file($uri)) {
      throw new \RuntimeException("The file $uri does not exist.");
    }
    if (!is_readable($uri)) {
      throw new \RuntimeException("The file $uri is not readable.");
    }

    $this->uri = $uri;
    parent::__construct([]);
    $this->read();
  }

  public function getUri() {
    return $this->uri;
  }

  public function read() {
    $contents = file_get_contents($this->uri);
    if ($contents === FALSE) {
      throw new \RuntimeException("Unable to read from {$this->uri}.");
    }

    $data = json_decode($contents, TRUE);
    if ($data === NULL && $error = json_last_error()) {
      $message = json_last_error_msg();
      throw new \RuntimeException("Error decoding JSON from {$this->uri}: {$message}.");
    }

    $this->setIndentation($contents);
    $this->exchangeArray($data);

    return $this;
  }

  public function write() {
    if (!is_writable($this->uri)) {
      throw new \RuntimeException("Unable to write to {$this->uri}.");
    }

    $data = $this->getArrayCopy();
    $contents = json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    if ($contents === FALSE && $error = json_last_error()) {
      $message = json_last_error_msg();
      throw new \RuntimeException("Error encoding JSON: {$message}.");
    }

    $this->fixIndentation($contents);

    // Ensure the file ends with a newline
    $contents .= PHP_EOL;

    $results = file_put_contents($this->uri, $contents);
    if ($results === FALSE) {
      throw new \RuntimeException("Error writing to {$this->uri}.");
    }

    return $this;
  }

  protected function setIndentation($contents) {
    if (preg_match('/^{\n(\s+)/', $contents, $matches)) {
      $this->indentation = $matches[1];
    }
  }

  protected function fixIndentation(&$contents) {
    if ($this->indentation !== static::DEFAULT_INDENTATION) {
      $contents = preg_replace_callback('/^(?:' . preg_quote(static::DEFAULT_INDENTATION, '/') . ')+/m', function($matches) {
        return str_repeat($this->indentation, strlen($matches[0]) / 4);
      }, $contents);
    }
  }

}
