<?php

namespace Drush\patch\PatchEngine;

use Consolidation\SiteProcess\ProcessManager;
use Consolidation\SiteProcess\ProcessManagerAwareInterface;
use Consolidation\SiteProcess\ProcessManagerAwareTrait;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\Process;

abstract class PatchEngine implements PatchEngineInterface, LoggerAwareInterface, ProcessManagerAwareInterface {

  use LoggerAwareTrait;
  use ProcessManagerAwareTrait;

  /**
   * @var \Drush\patch\PatchEngine\Patch[]
   */
  protected $patches = [];

  /**
   * Constructs a new PatchEngine.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(LoggerInterface $logger, ProcessManager $processManager = NULL) {
    $this->logger = $logger;
    $this->processManager = $processManager;
  }

  public function create(ContainerInterface $container) {
    return new static(
      $container->get('logger'),
      $container->get('process.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPatches() {
    return $this->patches;
  }

  /**
   * {@inheritdoc}
   */
  public function getPatch($project, $id) {
    foreach ($this->patches as $patch) {
      if ($patch->project === $project && $patch->id === $id) {
        return $patch;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setPatches(array $patches) {
    $this->patches = $patches;
  }

  /**
   * {@inheritdoc}
   */
  public function getProjectPatches($project) {
    $project_patches = [];
    foreach ($this->patches as $patch) {
      if ($patch->project === $project) {
        $project_patches[] = $patch;
      }
    }
    return $project_patches;
  }

  /**
   * {@inheritdoc}
   */
  public function getPatchesStatus() {
    $patches = $this->patches;
    $applied_patches = $this->getAppliedPatches();

    $status = [];
    foreach ($patches as $patch) {
      if (!isset($patch->status)) {
        $patch->status = $patch->getStatus($applied_patches);
      }
      // Default status until it can be confirmed that it is applied.
      //      $patch->status = Patch::PATCH_NOT_APPLIED;
      //      if ($matchedPatch = $patch->findSimilarPatch($applied_patches)) {
      //        $patch->status = $patch->isSame($matchedPatch) ? Patch::PATCH_APPLIED : Patch::PATCH_NEEDS_UPDATE;
      //      }
      $status[] = $patch;
    }

    // Review for any currently applied patches that should be removed.
    foreach ($applied_patches as $applied_patch) {
      if (!isset($applied_patch->status)) {
        $applied_patch->status = $applied_patch->getStatus($patches, Patch::PATCH_NEEDS_REMOVAL);
      }
      if ($applied_patch->status !== PATCH::PATCH_APPLIED) {
        $status[] = $applied_patch;
      }
      //      if (!$applied_patch->findSimilarPatch($patches)) {
      //        $applied_patch->status = Patch::PATCH_NEEDS_REMOVAL;
      //        $status[] = $applied_patch;
      //      }
    }

    // Sort before returning.
    uasort($status, [Patch::class, 'sort']);

    return $status;
  }

  /**
   * {@inheritdoc}
   */
  public function getPatchedProjects() {
    $projects = array_unique(array_map(function (Patch $patch) {
      return $patch->project;
    }, $this->patches));
    sort($projects);
    return $projects;
  }

  /**
   * {@inheritdoc}
   */
  public function addPatch(Patch $patch, Process $process) {
    $this->patches[] = $patch;
  }

  /**
   * {@inheritdoc}
   */
  public function removePatch(Patch $patch, Process $process) {
    $this->patches = array_filter($this->patches, function (Patch $project_patch) use ($patch) {
      return !$project_patch->isSame($patch);
    });
  }

}
