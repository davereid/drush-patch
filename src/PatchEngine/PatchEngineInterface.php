<?php

namespace Drush\patch\PatchEngine;

use Symfony\Component\Process\Process;

interface PatchEngineInterface {

  /**
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  public static function isApplicable();

  /**
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  public function getPatches();

  /**
   * @param string $project
   *   The project name.
   * @param string $id
   *   The patch ID.
   *
   * @return \Drush\patch\PatchEngine\Patch
   */
  public function getPatch($project, $id);

  /**
   * @param string $project
   *   The project name.
   *
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  public function getProjectPatches($project);

  /**
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  public function getAppliedPatches();

  /**
   * @return \Drush\patch\PatchEngine\Patch[]
   */
  public function getPatchesStatus();

  /**
   * Get the list of available projects.
   *
   * @return string[]
   *   An array of project names.
   */
  public function getAllProjects();

  /**
   * Get the list of currently patched projects.
   *
   * @return string[]
   *   An array of project names.
   */
  public function getPatchedProjects();

  /**
   * Removes a patch.
   *
   * @param \Drush\patch\PatchEngine\Patch[] $patches
   */
  public function setPatches(array $patches);

  public function save();

  /**
   * @param \Drush\patch\PatchEngine\Patch $patch
   *   The patch to add.
   * @param \Symfony\Component\Process\Process $process
   *   The command line process to execute adding the patch.
   *
   * @return \Drush\patch\PatchEngine\Patch $patch
   */
  public function addPatch(Patch $patch, Process $process);

  /**
   * @param \Drush\patch\PatchEngine\Patch $patch
   *   The patch to remove.
   * @param \Symfony\Component\Process\Process $process
   *   The command line process to execute removing the patch.
   */
  public function removePatch(Patch $patch, Process $process);

  /**
   * @param \Symfony\Component\Process\Process $process
   *   The command line process to execute setting up this engine.
   */
  public function setUp(Process $process);

}
