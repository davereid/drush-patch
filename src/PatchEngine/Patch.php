<?php

namespace Drush\patch\PatchEngine;

class Patch {

  const PATCH_NOT_APPLIED = 'not applied';
  const PATCH_APPLIED = 'applied';
  const PATCH_NEEDS_UPDATE = 'needs update';
  const PATCH_NEEDS_REMOVAL = 'needs removal';

  public $project;
  public $id;
  public $uri;
  public $source;
  public $status;

  public static function sort(self $a, self $b) {
    return $a->project > $b->project;
  }

  /**
   * @param self[] $patchesToMatch
   *
   * @return self|false
   */
  public function findSimilarPatch(array $patchesToMatch) {
    $matching = array_filter($patchesToMatch, function(self $patch) {
      return $this->isSimilar($patch);
    });
    return reset($matching);
  }

  /**
   * @param self[] $patchesToMatch
   *
   * @return string
   */
  public function getStatus(array $patchesToCheck, $default = self::PATCH_NOT_APPLIED) {
    foreach ($patchesToCheck as $patch) {
      if ($patch->project === $this->project) {
        if ($patch->id === $this->id) {
          if ($patch->uri === $this->uri) {
            return self::PATCH_APPLIED;
          }
          //else {
          // return self::PATCH_NEEDS_UPDATE;
          //}
        }
        elseif ($patch->uri === $this->uri) {
          return self::PATCH_NEEDS_UPDATE;
        }
      }
    }
    return $default;
  }

  /**
   * @param self $patchToMatch
   *
   * @return bool
   */
  public function isSame(self $patch) {
    // The project and ID must match to be considered the same.
    return $this->project === $patch->project && $this->id === $patch->id;
  }

  /**
   * @param self $patchToMatch
   *
   * @return bool
   */
  public function isSimilar(self $patch) {
    // The project, label and uri properties must match to be considered the same.
    return $this->project === $patch->project && ($this->id === $patch->id || $this->uri === $patch->uri);
  }

  public function __toString() {
    return $this->id;
  }

}
